{
  "model": "StudentRemoveActions",
  "title": "Student Removal",
  "settings": {
    "i18n": {
      "el": {
        "Student Removal": "Διαγραφή Φοιτητή",
        "Removal date cannot be earlier than the inscription date.": "Η ημερομηνία διαγραφής δεν μπορεί να είναι παλαιότερη απο την ημερομηνία εισαγωγής",
        "Removal year and period should be greater than inscription year and period.": "Το έτος και η περίοδος διαγραφής πρέπει να είναι μεταγενέστερα απο το έτος και την περίοδο εισαγωγής."
      }
    }
  },
  "components": [
    {
      "type": "fieldset",
      "legend": "Student Information",
      "components": [
        {
          "columns": [
            {
              "components": [
                {
                  "label": "Given Name",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "required": false
                  },
                  "key": "object.person.givenName",
                  "type": "textfield",
                  "input": true,
                  "disabled": true
                }
              ],
              "width": 5
            },
            {
              "components": [
                {
                  "label": "Family Name",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "required": false
                  },
                  "key": "object.person.familyName",
                  "type": "textfield",
                  "input": true,
                  "disabled": true
                }
              ],
              "width": 6
            }
          ],
          "customClass": "text-dark",
          "hideLabel": true,
          "type": "columns",
          "input": false
        },
        {
          "columns": [
            {
              "components": [
                {
                  "label": "Registration Number",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "required": false
                  },
                  "key": "object.studentIdentifier",
                  "type": "textfield",
                  "input": true,
                  "disabled": true
                }
              ],
              "width": 4
            },
            {
              "components": [
                {
                  "label": "Department Name",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "required": false
                  },
                  "key": "object.department.name",
                  "type": "textfield",
                  "input": true,
                  "disabled": true
                }
              ],
              "width": 7
            }
          ],
          "customClass": "text-dark",
          "hideLabel": true,
          "type": "columns",
          "input": false
        }
      ]
    },
    {
      "type": "fieldset",
      "legend": "Student Removal Information",
      "components": [
        {
          "columns": [
            {
              "components": [
                {
                  "label": "Removal Number",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "maxLength": 20
                  },
                  "key": "removalNumber",
                  "type": "textfield",
                  "input": true
                }
              ],
              "width": 3
            },
            {
              "components": [
                {
                  "label": "Removal Date",
                  "key": "removalDate",
                  "spellcheck": true,
                  "labelPosition": "top",
                  "enableTime": false,
                  "customOptions": {
                    "dateFormat": "yyyy-MM-dd",
                    "allowInput": true
                  },
                  "widget": {
                    "type": "calendar"
                  },
                  "validate": {
                    "required": true,
                    "custom": "valid = !!data.removalDate && !!data.object.inscriptionDate? new Date(data.object.inscriptionDate).getTime() - new Date(data.removalDate).getTime() <= 0: true;"
                  },
                  "errors": {
                    "custom": "Removal date cannot be earlier than the inscription date."
                  },
                  "type": "datetime",
                  "format": "dd/MM/yyyy",
                  "input": true,
                  "defaultValue": null
                }
              ],
              "width": 3
            },
            {
              "components": [
                {
                  "label": "Removal Year",
                  "labelPosition": "top",
                  "key": "removalYear",
                  "type": "select",
                  "searchEnabled": false,
                  "dataSrc": "url",
                  "data": {
                    "url": "AcademicYears?$filter=id ge {{data.object.inscriptionYear}}&$top={{limit}}&$skip={{skip}}&$orderby=id desc",
                    "headers": []
                  },
                  "limit": 20,
                  "template": "{{ item.alternateName }}",
                  "input": true,
                  "lazyLoad": true,
                  "dataType": "object",
                  "dataPath": "id",
                  "selectValues": "value",
                  "widget": "choicesjs",
                  "validate": {
                    "required": true
                  }
                }
              ],
              "width": 3
            },
            {
              "components": [
                {
                  "label": "Removal Period",
                  "labelPosition": "top",
                  "widget": "choicesjs",
                  "dataSrc": "url",
                  "data": {
                    "url": "AcademicPeriods?",
                    "headers": []
                  },
                  "validate": {
                    "required": true,
                    "custom": "valid = !!data.removalPeriod.id && !!data.object.inscriptionPeriod && data.removalYear.id === data.object.inscriptionYear? data.removalPeriod.id >= data.object.inscriptionPeriod : true;"
                  },
                  "errors": {
                    "custom": "Removal year and period should be greater than inscription year and period."
                  },
                  "template": "{{ item.name }}",
                  "dataType": "object",
                  "dataPath": "id",
                  "selectValues": "value",
                  "key": "removalPeriod",
                  "type": "select",
                  "input": true,
                  "searchEnabled": false
                }
              ],
              "width": 3
            },
            {
              "components": [
                {
                  "label": "Removal Decision",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "maxLength": 30
                  },
                  "key": "removalDecision",
                  "type": "textfield",
                  "input": true
                }
              ],
              "width": 6
            },
            {
              "components": [
                {
                  "label": "Removal Request",
                  "key": "removalRequest",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "required": false,
                    "maxLength": 15
                  },
                  "type": "textfield",
                  "input": true
                }
              ],
              "width": 6
            },
            {
              "components": [
                {
                  "label": "Removal Department",
                  "key": "removalDepartment",
                  "labelPosition": "top",
                  "validate": {
                    "required": false,
                    "maxLength": 70
                  },
                  "input": true,
                  "type": "textfield",
                  "clearOnRefresh": true,
                  "disabled": false
                }
              ],
              "width": 12,
              "offset": 0,
              "push": 0,
              "pull": 0
            },
            {
              "components": [
                {
                  "label": "Removal Reason",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "required": true,
                    "maxLength": 80
                  },
                  "key": "removalReason",
                  "type": "textarea",
                  "input": true
                }
              ],
              "width": 12
            },
            {
              "components": [
                {
                  "label": "Removal Comments",
                  "labelPosition": "top",
                  "widget": {
                    "type": "input"
                  },
                  "validate": {
                    "required": false,
                    "maxLength": 1024
                  },
                  "key": "removalComments",
                  "type": "textarea",
                  "input": true
                }
              ],
              "width": 12
            },
            {
              "components": [
                {
                  "label": "Status",
                  "widget": "choicesjs",
                  "dataSrc": "json",
                  "data": {
                    "json": [
                      {
                        "name": "Completed",
                        "alternateName": "CompletedActionStatus",
                        "id": 3
                      }
                    ]
                  },
                  "template": "{{ item.alternateName }}",
                  "key": "actionStatus",
                  "dataType": "object",
                  "idPath": "id",
                  "selectValues": "value",
                  "type": "select",
                  "input": true,
                  "hideOnChildrenHidden": false,
                  "calculateValue": "value = component.data.json[0];",
                  "clearOnHide": false,
                  "hidden": true         
                }
              ],
              "width": 4,
              "offset": 0,
              "push": 0,
              "pull": 0
            }
          ],
          "customClass": "text-dark",
          "hideLabel": true,
          "type": "columns",
          "input": false
        }
      ]
    },
    {
      "type": "button",
      "label": "Submit",
      "key": "submit",
      "disableOnInvalid": true,
      "input": true
    }
  ],
  "width": 3
}

