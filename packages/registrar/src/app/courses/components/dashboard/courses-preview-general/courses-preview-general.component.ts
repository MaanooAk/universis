import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-courses-preview-general',
  templateUrl: './courses-preview-general.component.html'
})
export class CoursesPreviewGeneralComponent implements OnInit, OnDestroy {

  @Input() model: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Courses')
        .where('id').equal(params.id)
        .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
        .getItem();
          // if replacedByCourse exists
      if (this.model.replacedByCourse) {
        // get course
        const replacedByCourse = await this._context.model('Courses')
            .where('id').equal(this.model.replacedByCourse)
            .select('displayCode, name')
            .getItem();
        // override model field
        this.model.replacedByCourse = replacedByCourse;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
