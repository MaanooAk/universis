import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class ActiveFieldGroupService {
    
    public group = new BehaviorSubject<{ id: string, name?: string, alternateName?: string, description?: string }>(null);
  
    constructor() {
    }
  
  }