import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserConsentAdminRoutingModule } from './user-consent-admin-routing.module';
import {
  ActivatedUser,
  ApplicationConfiguration,
  ConfigurationService,
  DiagnosticsService,
  SharedModule
} from '@universis/common';
import { RouterModule } from '@angular/router';
import { MostModule } from '@themost/angular';
import { AdvancedFormsModule } from '@universis/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import * as el from './i18n/user-consent.el.json';
import * as en from './i18n/user-consent.en.json';
// todo: create a shared library for settings and avoid using an internal dependency
import { SettingsSection, SettingsService } from '../settings-shared/services/settings.service';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';
import { FieldGroupOverviewComponent } from './components/field-group-overview/field-group-overview.component';
import { TablesModule } from '@universis/ngx-tables';
import { FormsModule } from '@angular/forms';
import { RouterModalModule } from '@universis/common/routing';
import { ParentFieldGroupResolver } from './resolvers';
import { ActiveFieldGroupService } from './active-field-group.service';
//

const translations = {
  el,
  en
};


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    TablesModule,
    AdvancedFormsModule,
    RouterModalModule,
    SettingsSharedModule,
    RouterModule,
    MostModule,
    UserConsentAdminRoutingModule
  ],
  declarations: [FieldGroupOverviewComponent]
})
export class UserConsentAdminModule {

  constructor(
    private translate: TranslateService,
    private settings: SettingsService,
    private configuration: ConfigurationService,
    private activatedUser: ActivatedUser,
    private diagnostics: DiagnosticsService) {
    this.configuration.loaded.subscribe((config: ApplicationConfiguration) => {
      const languages = config.settings.i18n.locales;
      languages.forEach((language: string) => {
        if (Object.prototype.hasOwnProperty.call(translations, language)) {
          this.translate.setTranslation(language, translations[language], true);
        }
      });
      setTimeout(() => {
        this.activatedUser.user.subscribe((user: any) => {
          if (!user) return;
          // the user is set only once
          this.diagnostics.hasService('UserConsentService').then((result) => {
            if (result) {
              this.settings.addSection(<SettingsSection>{
                category: 'Lists',
                name: 'ConsentFieldGroups',
                description: this.translate.instant('Settings.Lists.ConsentFieldGroups.Description'),
                longDescription: this.translate.instant('Settings.Lists.ConsentFieldGroups.LongDescription'),
                url: `/consents/configuration/groups`
              });
            }
          });
        });
      });
    });
  }

  static forRoot(): ModuleWithProviders<UserConsentAdminModule> {
    return {
      ngModule: UserConsentAdminModule,
      providers: [
        ActiveFieldGroupService,
        ParentFieldGroupResolver
      ]
    };
  }
}
