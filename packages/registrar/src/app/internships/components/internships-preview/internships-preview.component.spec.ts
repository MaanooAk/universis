import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternshipsPreviewComponent } from './internships-preview.component';

describe('InternshipsPreviewComponent', () => {
  let component: InternshipsPreviewComponent;
  let fixture: ComponentFixture<InternshipsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternshipsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternshipsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
