import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternshipsRootComponent } from './internships-root.component';

describe('InternshipsRootComponent', () => {
  let component: InternshipsRootComponent;
  let fixture: ComponentFixture<InternshipsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternshipsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternshipsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
