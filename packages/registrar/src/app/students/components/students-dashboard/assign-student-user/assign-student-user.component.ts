import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService, ToastService } from '@universis/common';
import { ButtonTypes, RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { ActiveDepartmentService } from '../../../../registrar-shared/services/activeDepartmentService.service';
import { Subscription } from 'rxjs';

import { isEmpty } from 'lodash';

enum UserClaimedState {
  'PotentiallyFree' = 'PotentiallyFree',
  'Claimed' = 'Claimed'
}

@Component({
  selector: 'app-assign-student-user',
  templateUrl: './assign-student-user.component.html',
  styleUrls: ['./assign-student-user.component.scss']
})
export class AssignStudentUserComponent extends RouterModalOkCancel implements OnInit, AfterViewInit, OnDestroy {
  private subscription: Subscription;
  private formChangeSubscription: Subscription;

  public student: any;
  public instituteConfiguration: any;
  public lastError: any;
  public userAlreadyClaimedByStudents: any;
  public userClaimedState: UserClaimedState | null;

  @ViewChild('formComponent') formComponent: AdvancedFormComponent;

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private context: AngularDataContext,
    private loadingService: LoadingService,
    private activeDepartment: ActiveDepartmentService,
    private errorService: ErrorService,
    private toastService: ToastService
  ) {
    // call super constructor
    super(router, activatedRoute);
    // set modal title
    this.modalTitle = this.translateService.instant('Students.AssignUserAction.Description');
    // and modal class
    this.modalClass = 'modal-lg';
  }

  async ngOnInit(): Promise<void> {
    this.subscription = this.activatedRoute.params.subscribe(async (params) => {
      try {
        this.loadingService.showLoading();
        // nullify last error (e.g route params change)
        this.lastError = null;
        // and reset ok button class
        this.okButtonClass = ButtonTypes.ok.buttonClass;
        // get potential existing student user to inform
        this.student = await this.context
          .model('Students')
          .where('id')
          .equal(params.id)
          .select('id', 'user/name as username', 'user/alternateName as userAlternateName')
          .getItem();
        // hide ok button if the student is linked with a user
        if (this.student && this.student.username) {
          this.okButtonClass = 'd-none';
        }
      } catch (err) {
        console.error(err);
        this.lastError = err;
      } finally {
        this.loadingService.hideLoading();
      }
    });
    try {
      // check if institute allows student user sharing (through institute configuration)
      const { organization } = await this.activeDepartment.getActiveDepartment();
      this.instituteConfiguration = await this.context
        .model('Institutes')
        .where('id')
        .equal(organization)
        .select('instituteConfiguration/allowStudentUserSharing as allowStudentUserSharing')
        .getItem();
    } catch (err) {
      console.error(err);
      this.errorService.navigateToError(err);
    }
  }

  async ngAfterViewInit(): Promise<void> {
    this.formChangeSubscription = this.formComponent.form.change.subscribe(async (event) => {
      try {
        // check if user is already assigned to another student (may be multiple)
        if (event && event.data && !isEmpty(event.data.user)) {
          try {
            this.loadingService.showLoading();
            this.userAlreadyClaimedByStudents = await this.context
              .model('Students')
              .where('id')
              .notEqual(this.student && this.student.id)
              .and('user')
              .equal(event.data.user.id)
              .select(
                'id',
                'studentIdentifier',
                'department/name as department',
                'person/familyName as familyName',
                'person/givenName as givenName',
                'studentStatus/alternateName as studentStatus'
              )
              .getItems();
            if (this.userAlreadyClaimedByStudents && this.userAlreadyClaimedByStudents.length) {
              this.userClaimedState = UserClaimedState.Claimed;
            } else {
              this.userClaimedState = UserClaimedState.PotentiallyFree;
            }
          } catch (err) {
            console.error(err);
            this.userClaimedState = UserClaimedState.PotentiallyFree;
          } finally {
            this.loadingService.hideLoading();
          }
        }
        if (event && event.data && !event.data.user) {
          // reset view if user is unselected
          this.userAlreadyClaimedByStudents = [];
          this.userClaimedState = null;
        }
        if (this.formComponent && this.formComponent.form.formio) {
          // ok button is only enabled if
          // the form data is valid
          // and the target user is PotentiallyFree
          // or it's claimed by another student and the institute allows student user sharing
          this.okButtonDisabled = !(
            this.formComponent.form.formio.checkValidity(null, false, null, true) &&
            (this.userClaimedState === UserClaimedState.PotentiallyFree ||
              (this.userClaimedState === UserClaimedState.Claimed &&
                this.instituteConfiguration &&
                this.instituteConfiguration.allowStudentUserSharing === true))
          );
        }
      } catch (err) {
        console.error(err);
        this.lastError = err;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  async ok() {
    try {
      if (this.student && this.student.username) {
        return this.cancel();
      }
      this.loadingService.showLoading();
      // nullify last error
      this.lastError = null;
      // get target user
      const user = this.formComponent.form.formio && this.formComponent.form.formio.data && this.formComponent.form.formio.data.user;
      if (isEmpty(user)) {
        return this.cancel();
      }
      // prepare minimal update object
      const updateStudentUser = {
        id: this.student && this.student.id,
        user
      };
      // update
      await this.context.model('Students').save(updateStudentUser);
      // close with reload fragment
      return this.close({ fragment: 'reload' }).then(() => {
        // and finally show a success toast
        const successToast: {
          Title: string;
          Message: string;
        } = this.translateService.instant('Students.AssignUserAction.SuccessToast');
        return this.toastService.show(successToast.Title, successToast.Message);
      });
    } catch (err) {
      console.error(err);
      this.lastError = err;
    } finally {
      this.loadingService.hideLoading();
    }
  }

  async cancel() {
    return this.close();
  }
}
