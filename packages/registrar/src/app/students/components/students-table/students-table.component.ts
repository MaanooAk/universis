import {AfterViewInit, Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Observable, Subscription} from 'rxjs';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { AppEventService, ConfigurationService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, UserActivityService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';
import {ClientDataQueryable} from '@themost/client';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';
import {ElotConverter} from '@universis/elot-converter';
import {GenitiveFormatter, GenderEnum} from '@universis/grammar';
import { StudentsService } from '../../services/students-service/students.service';
import { GraduationGradeWrittenInWordsCalculationAttributes } from '../../services/students-service/students.service';
import { StudentDegreeTemplateService } from '../../../degree-templates/services/student-degree-template.service';
import { ActiveTableQueryBuilder } from '../../../query-builder/active-table-query-builder';
import { STUDENT_COLUMNS_CONFIG as studentColumnsConfig} from './students-columns-config';
import { TableColumnConfiguration } from '@universis/ngx-tables';
import { containsElement } from '@angular/animations/browser/src/render/shared';

interface SeriesMapping {
  item: any,
  series: any,
}

@Component({
  selector: 'app-students-table',
  templateUrl: './students-table.component.html',
  styles: [`
      .dropdown-item-beta:after {
          content: 'BETA';
          padding-left: 8px;
          font-size: 0.7rem;
          color: var(--danger);
          font-weight: 500;
      }
    `],
  providers: [
    ActiveTableQueryBuilder
  ]
})
export class StudentsTableComponent implements OnInit, OnDestroy, AfterViewInit {
  private selectedItems = [];
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  public isLoading = true;
  public defaultLocale: string;
  public locale: string;

  @Input() department: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public recordsTotal: any;
  private fromProgram: any;
  private fromSpecialty: any;
  eventSubscription: Subscription;
  queryDescription: string;
  routeSubscription: Subscription;

  public hasSeries = false;

  constructor(
    public activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _activeDepartmentService: ActiveDepartmentService,
    private _configurationService: ConfigurationService,
    private _studentsService: StudentsService,
    public studentDegreeTemplate: StudentDegreeTemplateService,
    private appEvent: AppEventService,
    public activeQuery: ActiveTableQueryBuilder
  ) {
    this.locale = this._translateService.currentLang;
  }
  ngAfterViewInit(): void {
    this.routeSubscription = this.activatedRoute.queryParams.subscribe((queryParams) => {
      this._activatedTable.activeTable = this.table;
      this.activeQuery.find(queryParams).subscribe((item) => {
        this.queryDescription = item && item.description;
      });
    });
  }

  ngOnInit() {
    this.dataSubscription = this.activatedRoute.data.subscribe( data => {
      this.queryDescription = null;
      this.isLoading = true;
      this.defaultLocale = this._configurationService.settings
        && this._configurationService.settings.i18n
        && this._configurationService.settings.i18n.defaultLocale;
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        Object.assign(this.search.form, { department: this.activatedRoute.snapshot.data.department.id });
        /*this.search.formComponent.formLoad.subscribe((res: any) => {
          Object.assign(res, { department: this._activatedRoute.snapshot.data.department.id });
        });*/
        this.search.ngOnInit();
      }

      // set model parameter for withoutCounselor config
      if (data.tableConfiguration && data.tableConfiguration.model === "Students/withoutCounselors") {
        data.tableConfiguration.model += `?department=${this.activatedRoute.snapshot.data.department.id}`;
      }

      // check for series
      this._activeDepartmentService.getActiveDepartment().then(activeDepartment => {
        this._context.model('DepartmentStudentSeries').select('count(id) as total')
          .where('department').equal(activeDepartment.id)
          .getItem().then(series => {
            this.hasSeries = series && series.total > 0;
          })
      })
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
      if (this.table.config) {
        return this._userActivityService.setItem({
          category: this._translateService.instant('Students.StudentTitle'),
          description: this._translateService.instant(this.table.config.title),
          url: window.location.hash.substring(1),
          dateCreated: new Date()
        });
      }
    });
  }

  /**
   * Change study program for selected students
   */
  async changeStudyProgram() {
    try {
      this._loadingService.showLoading();
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      // change study program only for students that attend same specialty and study program
      let fromProgram =  [...items.map(item => item.studyProgram)];
      fromProgram = fromProgram.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      let fromSpecialty =  [...items.map(item => item.studyProgramSpecialty)];
      fromSpecialty = fromSpecialty.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromProgram.length !== 1 || fromSpecialty.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
        this.fromSpecialty = fromSpecialty;
      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-xl',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
            'currentSpecialty': this.fromSpecialty && this.fromSpecialty.length ? this.fromSpecialty[0] : null,
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/changeProgram' : null,
          modalTitle: 'Students.ChangeProgramAction.Title',
          description: 'Students.ChangeProgramAction.Description',
          errorMessage: 'Students.ChangeProgramAction.CompletedWithErrors',
          additionalMessage:  this._translateService.instant('Students.ChangeProgramAction.NoItems'),
          refresh: this.refreshAction,
          execute: this.executeChangeProgramAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Apply study program rules for selected students
   */
  async applyProgramRules() {
    try {
      this._loadingService.showLoading();
      let message = this._translateService.instant('Students.ApplyProgramRulesAction.NoItems');
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      // apply program rules only for students that attend same study program
      let fromProgram =  [...items.map(item => item.studyProgram)];
      fromProgram = fromProgram.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromProgram.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
        // check if study program has rules
        const rules = await this._context.model('StudyProgramReplacementRules').select('count(id) as total')
            .where('studyProgram').equal(this.fromProgram[0])
            .getItem();
        if (rules && rules.total === 0) {
          this.selectedItems = [];
          message = this._translateService.instant('Students.ApplyProgramRulesAction.NoRules');
        }

      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/applyProgramRules' : null,
          modalTitle: 'Students.ApplyProgramRulesAction.Title',
          description: 'Students.ApplyProgramRulesAction.Description',
          errorMessage: 'Students.ApplyProgramRulesAction.CompletedWithErrors',
          additionalMessage:  message,
          refresh: this.refreshAction,
          execute: this.executeApplyProgramRulesAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes update of specialty for students
   */
  executeApplyProgramRulesAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.studyProgramReplacementRule == null || data.studyProgramReplacementRule === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      const items = {
        students: this.selectedItems,
        data: {
          replacementRule: data.studyProgramReplacementRule
        }
      };
      this._context.model(`Students/applyProgramRules`).save(items).then(() => {
        result.success = result.total;
        observer.next(result);
      }).catch((err) => {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }
  /**
   * Change study program for selected students
   */
  async changeSpecialty() {
    try {
      this._loadingService.showLoading();
      let message = this._translateService.instant('Students.ChangeSpecialtyAction.NoItems');
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      // change study program only for students that attend same study program
      let fromProgram =  [...items.map(item => item.studyProgram)];
      fromProgram = fromProgram.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromProgram.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
          // check if study program has specialties
          const specialties = await this._context.model('StudyProgramSpecialties').select('count(id) as total')
            .where('studyProgram').equal(this.fromProgram[0])
            .and('specialty').notEqual(-1).getItem();
          if (specialties && specialties.total === 0) {
            this.selectedItems = [];
            message = this._translateService.instant('Students.ChangeSpecialtyAction.NoSpecialties');
          }

      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/changeSpecialty' : null,
          modalTitle: 'Students.ChangeSpecialtyAction.Title',
          description: 'Students.ChangeSpecialtyAction.Description',
          errorMessage: 'Students.ChangeSpecialtyAction.CompletedWithErrors',
          additionalMessage:  message,
          refresh: this.refreshAction,
          execute: this.executeChangeSpecialtyAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Set the student series for selected students
   */
  async setSeries() {
    try {
      this._loadingService.showLoading();
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      let message = this._translateService.instant('Students.SetSeriesAction.NoItems');
      let items = await this.getSelectedItems(item => ({
        id: item.student ? item.student.id : item.id,
        status: item.student ? item.student.studentStatus : item.studentStatus,
        studentSeries: item.student ? item.student.studentSeries : item.studentSeries,
      }));
      items = items.filter((item) => {
        return item.status === 'active';
      });
      items.forEach(i => i.studentSeries = i.studentSeries ? i.studentSeries.id || i.studentSeries : null);
      this.selectedItems = items;
      // check if department has series
      const series = await this._context.model('DepartmentStudentSeries').select('count(id) as total')
        .where('department').equal(activeDepartment.id)
        .getItem();
      if (series && series.total === 0) {
        this.selectedItems = [];
        message = this._translateService.instant('Students.SetSeriesAction.NoSeries');
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/setSeries' : null,
          modalTitle: 'Students.SetSeriesAction.Title',
          description: 'Students.SetSeriesAction.Description',
          errorMessage: 'Students.SetSeriesAction.CompletedWithErrors',
          additionalMessage: message,
          refresh: this.refreshAction,
          execute: this.executeSetSeries()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeSetSeries() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      const { overwriteExisting } = data;
      delete data.overwriteExisting;
      delete data.department;
      if (!overwriteExisting) {
        this.selectedItems = this.selectedItems
          .filter(i => i.studentSeries == null);
        result.success += result.total - this.selectedItems.length;
        if (this.selectedItems.length == 0) {
          observer.next(result);
          this._modalService.showWarningDialog(
            this._translateService.instant('Students.SetSeriesAction.AllSetTitle'),
            this._translateService.instant('Students.SetSeriesAction.AllSetMessage'),
            DIALOG_BUTTONS.Ok);
          return;
        }
      }
      if (data.studentSeries == null || data.studentSeries === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          this.refreshAction.emit({
            progress: Math.floor(((index + 1) / total) * 100)
          });
          try {
            const item = this.selectedItems[index];
            data.id = item.id;
            await this._context.model("Students").save(data);
            result.success += 1;
          } catch (err) {
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async distributeSeries() {
    try {
      this._loadingService.showLoading();
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      let message = this._translateService.instant('Students.DistributeSeriesAction.NoItems');
      let items = await this.getSelectedItemsWithFields([
        'id',
        'studentStatus/alternateName as status',
        'semester',
        // for the sorting
        'studentIdentifier',
        'person/familyName as familyName',
        'person/givenName as givenName'
      ]);
      let itemsWithSeries = await this.getSelectedItemsWithFields([
        "id",
        "studentSeries/id as series"
      ]);
      items = items.filter((item) => {
        return item.status === 'active';
      });
      // add the series field for the selected items if it exists
      for (const item of itemsWithSeries) {
        items.find(i => i.id == item.id).series = item.series;
      }
      this.selectedItems = items;
      // check if department has series
      const series = await this._context.model('DepartmentStudentSeries').select('id')
        .where('department').equal(activeDepartment.id)
        .take(-1)
        .getItems();
      if (series && series.length === 0) {
        this.selectedItems = [];
        message = this._translateService.instant('Students.DistributeSeriesAction.NoSeries');
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'department': activeDepartment.id,
            'series': series.map(i => i.id), // start will series selected
            'withSeries': series.filter(i => i.series).length
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/distributeSeries' : null,
          modalTitle: 'Students.DistributeSeriesAction.Title',
          description: 'Students.DistributeSeriesAction.Description',
          errorMessage: 'Students.DistributeSeriesAction.CompletedWithErrors',
          additionalMessage: message,
          refresh: this.refreshAction,
          execute: this.executeDistributeSeries()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeDistributeSeries() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      // check if student already has student group
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        const mappings = await this.distributeSeriesMapping(this.selectedItems, data);

        for (let index = 0; index < this.selectedItems.length; index++) {
          this.refreshAction.emit({
            progress: Math.floor(((index + 1) / total) * 100)
          });
          try {
            const item = this.selectedItems[index];
            const itemMapping = mappings.find(i => i.item.id == item.id);
            if (itemMapping) {
              const data = {
                id: item.id,
                studentSeries: itemMapping.series.id,
              }
              await this._context.model("Students").save(data);
              try {
                await this.table.fetchOne({
                  id: item.id
                });
              } catch (err) {
                //
              }
            }
            result.success += 1;
          } catch (err) {
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Options:
   * - series: the series, defaults to all department series
   * - balancePerSemester: count series totals per semester
   * - ignoreExisting: ignore the current member of the series
   * - overwriteExisting: replace existing series if already assigned
   * - sortingField: field to sort by the students before the distribution
   * - assignmentOrder: how from the sorted order will be distributed to the series
   */
  async distributeSeriesMapping(items: any[], options: any): Promise<SeriesMapping[]> {
    const {
      balancePerSemester,
      ignoreExisting,
      overwriteExisting,
      sortingField,
      assignmentOrder
    } = options;

    let series = await this._context.model('DepartmentStudentSeries')
      .select("id", "name")
      .orderBy("name")
      .take(-1)
      .getItems();
    if (options.series) {
      series = series.filter(i => options.series.some(id => i.id == id));
    }

    if (!overwriteExisting) {
      items = items.filter(i => !i.series);
    }

    const semesters = this.selectedItems
      .map(i => i.semester)
      .filter((i, index, array) => array.indexOf(i) == index)
      .sort((a, b) => a - b);

    let counts = [];
    if (balancePerSemester) {
      if (!ignoreExisting) {
        counts = await this._context.model('Students')
          .select('studentSeries/id as series', 'semester as semester', 'count(id) as total')
          .where('studentStatus/alternateName').equal('active')
          .groupBy('studentSeries/id', 'semester', 'studentStatus/alternateName')
          .take(-1)
          .getItems();
      }
      // fill missing
      semesters.forEach(semester => series.map(i => i.id).forEach(series => {
        if (!counts.find(i => i.semester == semester && i.series == series)) {
          counts.push({ series, semester, total: 0 })
        }
      }))
    } else {
      if (!ignoreExisting) {
        counts = await this._context.model('Students')
          .select('studentSeries/id as series', 'count(id) as total')
          .where('studentStatus/alternateName').equal('active')
          .groupBy('studentSeries/id', 'studentStatus/alternateName')
          .take(-1)
          .getItems();
      }
      // fill missing
      series.map(i => i.id).forEach(series => {
        if (!counts.find(i => i.series == series)) {
          counts.push({ series, total: 0 })
        }
      })
    }
    // remove irrelevant series
    counts = counts.filter(count => series.some(i => i.id == count.series))

    // sort the students
    let sortedItems;
    if (!sortingField || sortingField == "id") {
      sortedItems = items.sort((a, b) =>
        (a.studentIdentifier).localeCompare(b.studentIdentifier));
    } else if (sortingField == "name") {
      sortedItems = items.sort((a, b) =>
        (a.familyName + " " + a.givenName).localeCompare(b.familyName + " " + b.givenName));
    } else {
      throw `Unknown sorting field '${sortingField}'`;
    }

    // find the final additions to each series
    // the space field describes how many students will added to the series
    counts.forEach(i => i.space = 0);

    const mappings: SeriesMapping[] = [];
    for (const item of items) {
      // select the one with the min total
      const selected = counts
        .filter(i => !balancePerSemester || i.semester == item.semester)
        .reduce((min, i) => (!min || i.total < min.total) ? i : min, null);
      selected.total += 1;
      selected.space += 1;
    }

    // selection order picker
    let select;
    if (!assignmentOrder || assignmentOrder == "linear") {
      select = (counts) => counts
        .find(i => i.space > 0);

    } else if (assignmentOrder == "mixed") {
      select = (counts, itemIndex) => {
        if (itemIndex >= 0 && counts[itemIndex % counts.length].space > 0) return counts[itemIndex % counts.length];
        return counts.reduce((max, i) => (!max || i.space > max.space) ? i : max, null);
      }

    } else {
      throw `Unknown assignment order '${assignmentOrder}'`;
    }

    let index = 0;
    for (const item of sortedItems) {
      // select the one with the min total
      const selected = select(
        counts.filter(i => !balancePerSemester || i.semester == item.semester),
        semesters.length > 1 ? -1 : index++
      )
      selected.space -= 1;

      const series_item = series.find(i => i.id == selected.series);
      mappings.push({ item, series: series_item });
    }

    // console.log(mappings.map(i => `${i.item.id} ${i.item.familyName} => ${i.series.name}`))
    return mappings;
  }

  /**
   * clear the student series for selected students
   */
  async clearSeries() {
    try {
      this._loadingService.showLoading();
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      let errorMessage = null;
      let items = await this.getSelectedItems(item => ({
        id: item.student ? item.student.id : item.id,
        studentSeries: item.student ? item.student.studentSeries : item.studentSeries,
      }));
      items.forEach(i => i.studentSeries = i.studentSeries ? i.studentSeries.id || i.studentSeries : null);
      if (items.length == 0) {
        errorMessage = this._translateService.instant('Students.ClearSeriesAction.NoItems');
      }
      items = items.filter(i => i.studentSeries != null);
      if (items.length == 0) {
        errorMessage = this._translateService.instant('Students.ClearSeriesAction.NoItemsWithSeries');
      }
      this.selectedItems = items;
      this._loadingService.hideLoading();
      if (errorMessage) {
        this._modalService.showErrorDialog(
          this._translateService.instant('Students.ClearSeriesAction.Title'), errorMessage)
      } else {
        this._modalService.openModalComponent(AdvancedRowActionComponent, {
          class: 'modal-lg',
          keyboard: false,
          ignoreBackdropClick: true,
          initialState: {
            items: this.selectedItems,
            modalTitle: 'Students.ClearSeriesAction.Title',
            description: 'Students.ClearSeriesAction.Description',
            errorMessage: 'Students.ClearSeriesAction.CompletedWithErrors',
            refresh: this.refreshAction,
            execute: this.executeClearSeries()
          }
        });
      }
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeClearSeries() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          this.refreshAction.emit({
            progress: Math.floor(((index + 1) / total) * 100)
          });
          try {
            const item = this.selectedItems[index];
            await this._context.model("Students").save({
              id: item.id,
              studentSeries: null,
            });
            result.success += 1;
          } catch (err) {
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Add study program groups
   */
  async addProgramGroup() {
    try {
      this._loadingService.showLoading();
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      let message = this._translateService.instant('Students.AddProgramGroupAction.NoItems');
      // create exams can be made only for course classes that belong to same year and period
      let fromProgram =  [...items.map(item => item.studyProgram)];
      fromProgram = fromProgram.filter( function( item, index, inputArray ) {
        return inputArray.indexOf(item) === index;
      });
      if (fromProgram.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
        // check if study program has groups
        const groups = await this._context.model('ProgramGroups').select('count(id) as total').where('program').equal(this.fromProgram[0])
          .and('groupType/alternateName').equal('simple').and('parentGroup').notEqual(null).getItem();
        if (groups && groups.total === 0) {
          this.selectedItems = [];
          message = this._translateService.instant('Students.AddProgramGroupAction.NoGroups');
        }
      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
             'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addStudentGroup' : null,
          modalTitle: 'Students.AddProgramGroupAction.Title',
          description: 'Students.AddProgramGroupAction.Description',
          errorMessage: 'Students.AddProgramGroupAction.CompletedWithErrors',
          additionalMessage:  message,
          refresh: this.refreshAction,
          execute: this.executeAddProgramGroup()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Add document request
   */
  async addDocumentRequest() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addDocumentRequest' : null,
          modalTitle: 'Students.AddDocumentRequest.Title',
          description: 'Students.AddDocumentRequest.Description',
          errorMessage: 'Students.AddDocumentRequest.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeAddDocumentRequest()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  /**
   * Executes add document request for students
   */
  executeAddDocumentRequest() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.object == null || data.object === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load document request
            data.student = item.id;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`RequestDocumentActions`).save(data);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Check scholarship rules
   */
  async checkScholarshipRules() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/checkScholarshipRules' : null,
          modalTitle: 'Students.CheckScholarshipRulesAction.Title',
          description: 'Students.CheckScholarshipRulesAction.Description',
          errorMessage: 'Students.CheckScholarshipRulesAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCheckScholarshipRules()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes check scholarship rules for students
   */
  executeCheckScholarshipRules() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.scholarship == null || data.scholarship === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
        this._context.model(`Scholarships/${data.scholarship}/checkStudents`).save( this.selectedItems).then(() => {
        result.success = result.total;
        observer.next(result);
      }).catch((err) => {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  /**
   * Check graduation rules
   */
  async checkGraduationRules() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      // get only active students
      this.selectedItems = items.filter( (item) => {
        return item.status === 'active';
      });
      this._loadingService.hideLoading();
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CheckGraduationRulesAction.Title',
          description: 'Students.CheckGraduationRulesAction.Description',
          errorMessage: 'Students.CheckGraduationRulesAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCheckRules(activeDepartment.id, 'GraduationRule')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Check thesis rules
   */
  async checkThesisRules() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      // get only active students
      this.selectedItems = items.filter( (item) => {
        return item.status === 'active';
      });
      this._loadingService.hideLoading();
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CheckThesisRulesAction.Title',
          description: 'Students.CheckThesisRulesAction.Description',
          errorMessage: 'Students.CheckThesisRulesAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCheckRules(activeDepartment.id, 'ThesisRule')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Check internship rules
   */
  async checkInternshipRules() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      // get only active students
      this.selectedItems = items.filter( (item) => {
        return item.status === 'active';
      });
      this._loadingService.hideLoading();
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CheckInternshipRulesAction.Title',
          description: 'Students.CheckInternshipRulesAction.Description',
          errorMessage: 'Students.CheckInternshipRulesAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeCheckRules(activeDepartment.id, 'InternshipRule')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes check graduation rules for students
   */
  executeCheckRules(department, ruleType) {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      this.refreshAction.emit({
        progress: 1
      });

      this._context.model(`departments/${department}/validateStudents`).save({  items: this.selectedItems, ruleType: ruleType}).then(() => {
        result.success = result.total;
        observer.next(result);
      }).catch((err) => {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }
  /**
   * Executes add program group for students
   */
  executeAddProgramGroup() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      // check if student already has student group
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.group == null || data.group === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load program group
            const group = await this._context.model('ProgramGroups').where('id').equal(data.group).getItem();
            data.object = item.id;
            data.groups = [group];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const exists = await this._context.model('StudentProgramGroups')
              .where('student').equal(item.id).and('programGroup').equal(data.group).select('id').getItem();
            if (!exists) {
              const action = await this._context.model(`UpdateStudentGroupActions`).save(data);
              if (action && action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
                result.success += 1;
              } else {
                result.errors += 1;
              }
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
  /**
   * Executes update of specialty for students
   */
  executeChangeSpecialtyAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.specialty == null || data.specialty === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            data.object = item.id;
            data.specialty = data.specialty;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`UpdateSpecialtyActions`).save(data);
            if (action && action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
              result.success += 1;
              try {
                await this.table.fetchOne({
                  id: item.id
                });
              } catch (err) {
                //
              }
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
  /**
   * Executes change program action for students
   */
  executeChangeProgramAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.specialty == null || data.studyProgram == null || data.specialty === '' || data.studyProgram === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            data.object = item.id;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model(`UpdateProgramActions`).save(data);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  /**
   * Add student counselor
   */
  async addStudentCounselor() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItemsWithFields([
        'id', 'studentStatus/alternateName as status', "inscriptionYear"
      ])
      // get only active students
      this.selectedItems = items.filter( (item) => {
        return item.status === 'active';
      });
      const latestInscriptionYear = this.selectedItems.map(i => i.inscriptionYear)
        .reduce((a, b) => a.id > b.id ? a : b); // max by id
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          data: {
            latestInscriptionYear
          },
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addStudentCounselor' : null,
          modalTitle: 'Students.AddStudentCounselorAction.Title',
          description: 'Students.AddStudentCounselorAction.Description',
          errorMessage: 'Students.AddStudentCounselorAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeAddStudentCounselor()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  /**
   * Executes add student counselors
   */
  executeAddStudentCounselor() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (!data.instructor.hasOwnProperty('id')
        || !data.fromYear.hasOwnProperty('id') || !data.fromPeriod.hasOwnProperty('id')
        || data.startDate == null || data.startDate === '' ) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      // option to replace existing assignments
      const disableExisting = data.disableExisting;
      delete data.disableExisting;
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            const studentId = item.id;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const update = [];
            const actives = await this._context.model("StudentCounselors")
              .where("student").equal(studentId)
              .and("active").equal(true)
              .getItems();
            // find selected counselor if already exists
            let target = actives
              .find(i => (i.instructor.id || i.instructor) == (data.instructor.id || data.instructor));
            // disable existing counselors but no the selected
            if (disableExisting) {
              for (const active of actives) {
                if (active == target) continue;
                active.active = false;
                active.toYear = data.toYear;
                active.toPeriod = data.toPeriod;
                active.endDate = data.endDate;
                update.push(active);
              }
            }
            // create or update the selected counselor
            target = Object.assign(target || {}, {
              student: studentId,
              instructor: data.instructor,
              active: true,
              fromYear: data.fromYear,
              fromPeriod: data.fromPeriod,
              startDate: data.startDate,
            });
            update.push(target);
            await this._context.model(`StudentCounselors`).save(update);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Add student counselor
   */
  async distributeStudentCounselor() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItemsWithFields([
        'id', 'studentStatus/alternateName as status', "inscriptionYear"
      ])
      // get only active students
      this.selectedItems = items.filter((item) => {
        return item.status === 'active';
      });
      const latestInscriptionYear = this.selectedItems.map(i => i.inscriptionYear)
        .reduce((a, b) => a.id > b.id ? a : b); // max by id
      this._loadingService.hideLoading();
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          data: {
            department: activeDepartment.id,
            departments: [activeDepartment.id],
            latestInscriptionYear
          },
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/distributeStudentCounselor' : null,
          modalTitle: 'Students.DistributeStudentCounselorAction.Title',
          description: 'Students.DistributeStudentCounselorAction.Description',
          errorMessage: 'Students.DistributeStudentCounselorAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeDistributeStudentCounselor()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Executes add student counselors
   */
  executeDistributeStudentCounselor() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;

      if (data.departments.length == 0
        || !data.fromYear.hasOwnProperty('id') || !data.fromPeriod.hasOwnProperty('id')
        || data.startDate == null || data.startDate === '') {
        this.selectedItems = [];

        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {

        const instructorFields = [
          "id", "familyName", "givenName", "category", "email"
        ]
        let instructors: any = [];
        if (data.criteria == "all") {
          instructors = await Promise.all(
            data.departments.map(department => {
              return this._context.model("Instructors")
                .select(...instructorFields)
                .where("department").equal(department)
                .and("status").equal(1)
                .expand("students")
                .take(-1)
                .getItems()
            }))
          instructors = instructors.flatMap(i => i);
        } else if (data.criteria == "instructors") {
          instructors = await Promise.all(
            data.instructors.map(instructor => {
              return this._context.model("Instructors")
                .select(...instructorFields)
                .where("id").equal(instructor.id)
                .expand("students")
                .getItem()
            }))
        } else if (data.criteria == "categories") {
          instructors = await Promise.all(
            data.departments.flatMap(department =>
              data.categories.map(category => {
                return this._context.model("Instructors")
                  .select(...instructorFields)
                  .where("department").equal(department)
                  .and("category").equal(category.category)
                  .and("status").equal(1)
                  .expand("students")
                  .take(-1)
                  .getItems()
              })))
          instructors = instructors.flatMap(i => i);
        }

        // if instructor has no consulted students, the students fields is expanded to an empty array
        for (const instructor of instructors) {
          // transform from an optional a array to a single number
          instructor.students = instructor.students.length ? instructor.students[0].totalStudents : 0;
        }

        for (let index = 0; index < this.selectedItems.length; index++) {
          this.refreshAction.emit({
            // other half of the progress bar
            progress: Math.floor(((index + 1) / this.selectedItems.length) * 100)
          });

          try {
            const item = this.selectedItems[index];
            const counselorResult = await this._context.model("StudentCounselors")
              .select("instructor")
              .where("student").equal(item.id)
              .and("active").equal(true)
              .getItem();
            if (counselorResult) {
              // skip if student has already assigned a counselor
              result.errors += 1;
              continue;
            }

            const selectedInstructor = instructors.reduce((min, i) =>
              i.students < min.students ? i : min, instructors[0]);
            const itemData = {
              student: item.id,
              instructor: selectedInstructor.id,
              active: true,
              fromYear: data.fromYear.id,
              fromPeriod: data.fromPeriod.id,
              startDate: data.startDate,
            }
            const action = await this._context.model(`StudentCounselors`).save(itemData);
            result.success += 1;
            selectedInstructor.students += 1; // increment if successful
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  /**
   * Add related department snapshot
   */
  async addDepartmentSnapshot() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          data: {
            'department': activeDepartment.id
          },
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addDepartmentSnapshot' : null,
          modalTitle: 'Students.AddDepartmentSnapshotAction.Title',
          description: 'Students.AddDepartmentSnapshotAction.Description',
          errorMessage: 'Students.AddDepartmentSnapshotAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeAddDepartmentSnapshot()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  /**
   * Executes add student counselors
   */
  executeAddDepartmentSnapshot() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.departmentSnapshot == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load document request
            data.id = item.id;
            data.departmentSnapshot = data.departmentSnapshot;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`Students`).save(data);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
  async calculateSemester() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active students
      this.selectedItems = items.filter( (item) => {
        return item.status === 'active';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CalculateSemesterAction.Title',
          description: 'Students.CalculateSemesterAction.Description',
          refresh: this.refreshAction,
          execute: this.executeCalculateSemester()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Calculates semester for selected students
   */
  executeCalculateSemester() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const studentSemester = await this._context.model(`Students/${item.id}/currentSemester`).getItems();
            if (studentSemester && item.semester !== studentSemester.value) {
              item.semester = studentSemester.value;
              await this._context.model('Students').save(item);
              result.success += 1;
            }
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async convertToElot() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItemsWithLocales();
      const elotMap = new Map<number, {familyName: string, givenName: string,
        fatherName: {isNull: boolean, value: string},
        motherName: {isNull: boolean, value: string}, enLocale: any}>();
      // validate familyName, givenName, fatherName, motherName
      this.selectedItems = items.filter(student => {
        const convertedFamilyName = ElotConverter.convert(student.person.familyName);
        const convertedGivenName = ElotConverter.convert(student.person.givenName);
        const convertedFatherName = {isNull: true, value: null};
        const convertedMotherName = {isNull: true, value: null};
        // father name and mother name are nullable
        // validate before passing to elot converter, which throws error for null input
        if (student.person.fatherName != null) {
          Object.assign(convertedFatherName, {
            isNull: false,
            value: ElotConverter.convert(student.person.fatherName)
          });
        }
        if (student.person.motherName != null) {
          Object.assign(convertedMotherName, {
            isNull: false,
            value: ElotConverter.convert(student.person.motherName)
          });
        }
        // try to find en locale
        const enLocale = student.person.locales.find(locale => locale.inLanguage === 'en');
        // add to map to save conversion for later
        elotMap.set(student.id, {familyName: convertedFamilyName, givenName: convertedGivenName,
          fatherName: convertedFatherName, motherName: convertedMotherName, enLocale: enLocale});
        if (enLocale) {
          return (enLocale.familyName !== convertedFamilyName
              || enLocale.givenName !== convertedGivenName
              || (!convertedFatherName.isNull && enLocale.fatherName !== convertedFatherName.value)
              || (!convertedMotherName.isNull && enLocale.motherName !== convertedMotherName.value));
        }
        return student;
      });
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.ConvertToElotAction.Title',
          description: 'Students.ConvertToElotAction.Description',
          errorMessage: 'Students.ConvertToElotAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeConvertToElotAction(elotMap)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeConvertToElotAction(elotMap: Map<number,
    {familyName: string, givenName: string, fatherName: {isNull: boolean, value: string},
    motherName: {isNull: boolean, value: string}, enLocale: any}>) {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // get converted value from map
            const convertedValues = elotMap.get(item.id);
            // update locales
            if (convertedValues.enLocale) {
              // if locale exists, update it
              Object.assign(convertedValues.enLocale, {
                familyName: convertedValues.familyName,
                givenName: convertedValues.givenName,
                fatherName: !convertedValues.fatherName.isNull ? convertedValues.fatherName.value : null,
                motherName: !convertedValues.motherName.isNull ? convertedValues.motherName.value : null
              });
            } else {
              // create en locale
              if (Array.isArray(item.person.locales)) {
                item.person.locales.push({
                  inLanguage: 'en',
                  familyName: convertedValues.familyName,
                  givenName: convertedValues.givenName,
                  fatherName: !convertedValues.fatherName.isNull ? convertedValues.fatherName.value : null,
                  motherName: !convertedValues.motherName.isNull ? convertedValues.motherName.value : null
                });
              }
            }
            // and save
            await this._context.model('Students').save(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async changeStatus() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only erased, declared or graduated students
      this.selectedItems = items.filter((item) => {
        return ['erased', 'declared', 'graduated'].includes(item.status);
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.ChangeStatusAction.Title',
          description: 'Students.ChangeStatusAction.Description',
          errorMessage: 'Students.ChangeStatusAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeChangeStatus()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeChangeStatus() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model('UpdateStudentStatusActions').save({
              object: item.id,
              studentStatus: {
                alternateName: 'active'
              }
            });
            if (action && action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
              result.success += 1;
              try {
                if (this.table.config && this.table.config.model === 'StudentDeclarations') {
                  // adapt item student for fetch one
                  item.student = item.id;
                  await this.table.fetchOne({
                    student: item.student
                  });
                } else {
                  await this.table.fetchOne({
                    id: item.id
                  });
                }
              } catch (err) {
                //
              }
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems(selectedItemsMapper?: (value: any, index: any, array: any) => any) {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          let selectArguments = [
            'id',
            'studentStatus/alternateName as status',
            'semester',
            'studyProgram/id as studyProgram',
            'studyProgramSpecialty/id as studyProgramSpecialty',
            'user',
            'graduationGrade',
            'studyProgram/decimalDigits as studyProgramDecimalDigits',
            'graduationGradeScale/id as graduationGradeScale',
            'graduationGradeScale/scaleType as graduationGradeScaleType',
            'graduationGradeWrittenInWords',
            'studentSeries'
          ];
          if (this.table.config.model === 'StudentDeclarations' || this.table.config.model === 'StudentSuspensions') {
            selectArguments = [
              'student/id as id',
              'student/studentStatus/alternateName as status',
              'student/semester as semester',
              'student/studyProgram/id as studyProgram',
              'student/studyProgramSpecialty/id as studyProgramSpecialty',
              'student/user as user'
            ];
          }
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // prioritize specific mapper for actions that are bound to specific lists, e.g the graduated students list
          // so other actions that do not fetch the data in their configs don't have problems saving the items
          items = selectedItemsMapper ? this.table.selected.map(selectedItemsMapper) :
          // get selected items only
          this.table.selected.map( (item) => {
            return {
              id: item.id,
              status: item.studentStatus,
              semester: item.semester,
              studyProgram : item.studyProgramId,
              studyProgramSpecialty: item.studyProgramSpecialtyId,
              user: item.user
            };
          });
        }
      }
    }
    return items;
  }

  async getSelectedItemsWithFields(selectArguments: string[]) {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        // query items
        const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
          .take(-1)
          .skip(0)
          .getItems();
        if (!this.table.smartSelect && this.table.selected) {
          items = queryItems.filter(item => {
            return this.table.selected.findIndex((x) => {
              return x.id === item.id;
            }) >= 0;
          });
        } else if (this.table.smartSelect && this.table.unselected && this.table.unselected.length) {
          // remove items that have been unselected by the user
          items = queryItems.filter(item => {
            return this.table.unselected.findIndex((x) => {
              return x.id === item.id;
            }) < 0;
          });
        } else {
          items = queryItems;
        }
      }
    }
    return items;
  }

  async createUser() {
    try {
      this._loadingService.showLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      if (!(activeDepartment && activeDepartment.departmentConfiguration
        && activeDepartment.departmentConfiguration.studentUsernameFormat)) {
        this._loadingService.hideLoading();
        // show informative message to the user about the required settings, if they are not set
        return this._modalService.showInfoDialog(this._translateService.instant('Students.CreateUserAction.SettingsRequired'),
          this._translateService.instant('Students.CreateUserAction.SettingsRequiredHelp'));
      }
      const items = await this.getSelectedItems();
      // get only active students
      this.selectedItems = items.filter(item => item.user == null);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CreateUserAction.Title',
          description: 'Students.CreateUserAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Students.CreateUserAction.CompletedWithErrors',
          execute: this.executeCreateUser()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async unlinkUser() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(item => item.user != null);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.UnlinkUserAction.Title',
          description: 'Students.UnlinkUserAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Students.UnlinkUserAction.CompletedWithErrors',
          execute: this.executeUnlinkUser()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeUnlinkUser() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const unlinkUser = {
              id: item.id,
              user: null,
              $state: 2
            };
            // unlink user
            await this._context.model('Students').save(unlinkUser);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executeCreateUser() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // create user
            await this._context.model(`Students/${item.id}/CreateUser`).save(null);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async convertToGenitive() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItemsWithLocales();
      // filter out items that have both genitives already filled
      // also validate father and mother names, so there is no confusion for null values
      this.selectedItems = items.filter(item => item.person
        && ((item.person.fatherName != null && item.person.fatherNameGenitive == null)
        || (item.person.motherName != null && item.person.motherNameGenitive == null)));
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.ConvertToGenitiveAction.Title',
          description: 'Students.ConvertToGenitiveAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Students.ConvertToGenitiveAction.CompletedWithErrors',
          execute: this.executeConvertToGenitive()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeConvertToGenitive() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            let locale: string;
            // modify only blank names
            if (item.person.motherNameGenitive == null && item.person.motherName != null) {
              locale = /^[A-Za-z0-9 ]*$/.test(item.person.motherName) ? 'en' : 'el';
              item.person.motherNameGenitive = GenitiveFormatter.format(item.person.motherName, locale, {
                gender: GenderEnum.Female
              });
            }
            if (item.person.fatherNameGenitive == null && item.person.fatherName != null) {
              locale = /^[A-Za-z0-9 ]*$/.test(item.person.fatherName) ? 'en' : 'el';
              item.person.fatherNameGenitive = GenitiveFormatter.format(item.person.fatherName, locale, {
                gender: GenderEnum.Male
              });
            }
            // and save
            await this._context.model('Students').save(item);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async calculateGraduationGradeWrittenInWords() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems((value: any, index: any, array: any) => {
        return {
          id: value.id,
          status: value.studentStatus,
          graduationGrade: value.graduationGrade,
          studyProgramDecimalDigits: value.studyProgramDecimalDigits,
          graduationGradeScale: value.graduationGradeScale,
          graduationGradeScaleType: value.graduationGradeScaleType,
          graduationGradeWrittenInWords: value.graduationGradeWrittenInWords
        };
      });
      this.selectedItems = items.filter(item =>
        // consider graduated students
        item.status === 'graduated'
        // ensure that graduationGradeWrittenInWords property exists
        && item.hasOwnProperty('graduationGradeWrittenInWords')
        // but is not set
        && !item.graduationGradeWrittenInWords);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CalculateGradeWrittenInWordsAction.Title',
          description: 'Students.CalculateGradeWrittenInWordsAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Students.CalculateGradeWrittenInWordsAction.CompletedWithErrors',
          execute: this.executeCalculateGraduationGradeWrittenInWords()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeCalculateGraduationGradeWrittenInWords() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // prepare calculation attributes
            const calculationAttributes: GraduationGradeWrittenInWordsCalculationAttributes = {
              studentStatus: item.status,
              graduationGrade: item.graduationGrade,
              decimalDigits: item.studyProgramDecimalDigits,
              graduationGradeScale: {
                id: item.graduationGradeScale,
                scaleType: item.graduationGradeScaleType
              }
            };
            // calculate graduation grade written in words
            const graduationGradeWrittenInWords = await
              this._studentsService.calculateGraduationGradeWrittenInWords(calculationAttributes);
            if (graduationGradeWrittenInWords) {
              // and update student
              const updateStudent = {
                id: item.id,
                graduationGradeWrittenInWords: graduationGradeWrittenInWords,
                $state: 2
              };
              await this._context.model('Students').save(updateStudent);
              try {
                await this.table.fetchOne({
                  id: item.id
                });
              } catch (err) {
                //
              }
            }
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Deletes selected students
   */
  async deleteAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active students
      this.selectedItems =  items.filter( (item) => {
        return item.status === 'active';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.DeleteAction.Title',
          description: 'Students.DeleteAction.Description',
          errorMessage: 'Students.DeleteAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeDeleteAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes copy action for course classes
   */
  executeDeleteAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // create user
            await this._context.model('Students').remove({id: item.id});
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItemsWithLocales() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          let selectArguments = ['id', 'person'];
          let expandExpression = 'person($expand=locales)';
          let shouldAdaptResultsMapping = false;
          if (this.table && this.table.config &&
            (this.table.config.model === 'StudentDeclarations' || this.table.config.model === 'StudentSuspensions')) {
              // reset select arguments
              selectArguments = [''];
              // alter expand expression
              expandExpression = 'student($expand=person($expand=locales))';
              // set to adapt mapping
              shouldAdaptResultsMapping = true;
            }
          // query items
          let queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .expand(expandExpression)
            .skip(0)
            .getItems();
          if (shouldAdaptResultsMapping) {
            // map items to follow elot action pattern
            queryItems = queryItems.map(queryItem => {
              return {
                id: queryItem.student.id,
                person: queryItem.student.person
              };
            });
          }
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          const itemPromises = this.table.selected.map(async item => await this._context.model('Students')
            .where('id').equal(item.id).select('id, person').expand('person($expand=locales)').getItem());
          items = await Promise.all(itemPromises);
        }
      }
    }
    return items;
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.eventSubscription) {
      this.eventSubscription.unsubscribe();
    }
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
  }

  onLoading(event: { target: AdvancedTableComponent }) {
    if (event && event.target && event.target.config) {
      // clone shared columns
      const clonedSharedColumns = JSON.parse(JSON.stringify(studentColumnsConfig));
      const configColumns = event.target.config.columns || [];
      if (['Students.Declared', 'Students.Suspended'].includes(event.target.config.title)) {
        clonedSharedColumns.forEach((column: TableColumnConfiguration) => {
          // adapt name and property to StudentDeclarations/StudentSuspensions
          column.name = `student/${column.name}`;
          column.property = `student${column.property.charAt(0).toUpperCase() + column.property.slice(1)}`;
          // try to find column in the config by name
          const findColumn = configColumns.find((configColumn) => configColumn.name === column.name);
          // if it does not exist
          if (findColumn == null) {
            // push it
            configColumns.push(column);
          }
        });
      } else {
        clonedSharedColumns.forEach((column: TableColumnConfiguration) => {
          // try to find column in the config by name
          const findColumn = configColumns.find((configColumn) => configColumn.name === column.name);
          // if it does not exist
          if (findColumn == null) {
            // push it
            configColumns.push(column);
          }
        });
      }
      if (event.target.config.title === 'Students.Graduated') {
        if (this.studentDegreeTemplate.enabled === false) {
          return;
        }
        const find = event.target.config.columns.find((item) => item.name === 'degreeTemplate/title');
        if (find == null) {
          event.target.config.columns.push({
            'name': 'degreeTemplate/title',
            'property': 'degreeTemplateTitle',
            'title': 'Settings.Attributes.degreeTemplateTitle',
            'formatters': [
              {
                'formatter': 'TemplateFormatter',
                // tslint:disable-next-line:max-line-length
                'formatString': '${degreeTemplateTitle != null ? degreeTemplateTitle : (studyProgramSpecialty.degreeTemplate != null ? studyProgramSpecialty.degreeTemplate.title : \'DegreeTemplates.NotSet\')}'
              },
              {
                'formatter': 'TranslationFormatter',
                'formatString': '${value}'
              },
              {
                'formatter': 'NgClassFormatter',
                'formatOptions': {
                  'ngClass': {
                    'text-secondary font-italic font-xs': '${degreeTemplateTitle == null}'
                  }
                }
              }
            ]
          });
          if (event.target.config.defaults.expand) {
            event.target.config.defaults.expand += ',';
          } else {
            event.target.config.defaults.expand = '';
          }
          event.target.config.defaults.expand += 'studyProgramSpecialty($select=id;$expand=degreeTemplate($select=id,title))';

          // add filter
          event.target.config.criteria.push({
            'name': 'hasDegreeTemplate',
            'filter': '(degreeTemplate/id ${value === 1 ? \'ne\' : \'eq\'} null)',
             'type': 'text'
          });
        }
      }
    }
  }

  onSearchLoading(event: { target: AdvancedSearchFormComponent }) {
    if (this.studentDegreeTemplate.enabled === false) {
      return;
    }
    const shouldAdd = !!(this.table && this.table.config && this.table.config.title === 'Students.Graduated');
    if (shouldAdd === false) {
      return;
    }
    const form = event && event.target && event.target.form;
    if (form && form.components) {
      const container = form.components[0].columns;
      // tslint:disable-next-line:max-line-length
      const hasDegreeTemplateComponentIndex = container.findIndex((item: any) => item.components && item.components[0].key === 'hasDegreeTemplate');
      if (hasDegreeTemplateComponentIndex < 0) {
        // tslint:disable-next-line:max-line-length
        const graduationPeriodComponentIndex = container.findIndex((item: any) => item.components && item.components[0].key === 'graduationPeriod');
        if (graduationPeriodComponentIndex >= 0) {
          container.splice(graduationPeriodComponentIndex + 1, 0, {
            'components': [
              {
                'label': 'Has degree template',
                'widget': 'choicesjs',
                'data': {
                  'values': [
                    {
                      'label': 'No',
                      'value': '0'
                    },
                    {
                      'label': 'Yes',
                      'value': '1'
                    }
                  ]
                },
                'selectThreshold': 0.3,
                'validate': {
                  'unique': false,
                  'multiple': false
                },
                'key': 'hasDegreeTemplate',
                'valueProperty': 'value',
                'selectValues': 'value',
                'type': 'select',
                'input': true,
                'searchEnabled': false,
                'hideOnChildrenHidden': false,
                'disableLimit': false,
                'lazyLoad': false
              }
            ],
            'width': 3,
            'offset': 0,
            'push': 0,
            'pull': 0
          });
        }
      }
    }
  }

}
