import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-users-dashboard-overview-departments',
  templateUrl: './users-dashboard-overview-departments.component.html',
  styleUrls: ['./users-dashboard-overview-departments.component.scss']
})
export class UsersDashboardOverviewDepartmentsComponent implements OnInit {

  @Input() user: any;

  constructor() { }

  ngOnInit() {
  }

}
