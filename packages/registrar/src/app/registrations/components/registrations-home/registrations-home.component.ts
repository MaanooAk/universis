import { AfterViewInit, Component, DoCheck, OnDestroy, OnInit } from '@angular/core';
import * as REGISTRATIONS_LIST_CONFIG from '../registrations-table/registrations-table.config.list.json';
import { Observable, ReplaySubject, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AppEventService } from '@universis/common';
import { ActiveTableQueryBuilder } from '../../../query-builder/active-table-query-builder';
import { QueryProperty } from '@universis/ngx-query-builder';
import { UserQuery } from '../../../query-builder/model';
import { ApplicationDatabase } from '../../../registrar-shared/services/app-db.service';
import { ActivatedTableService, AdvancedTableComponent } from '@universis/ngx-tables';
import { TranslateService } from '@ngx-translate/core';
import * as registrationBuilder from '../registrations-table/registrations-query-builder';
import * as studentBuilder from '../../../students/components/students-table/students-query-builder';
import { cloneDeep } from 'lodash';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'app-registrations-home',
  templateUrl: './registrations-home.component.html',
  styleUrls: ['./registrations-home.component.scss'],
  providers: [
    ActiveTableQueryBuilder
  ]
})
export class RegistrationsHomeComponent implements OnInit, AfterViewInit, OnDestroy {

  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public maxActiveTabs = 3;
  public table: AdvancedTableComponent;
  public tabs: Array<any> = [];
  private paramSubscription: Subscription;
  private toggleShowExportSubscription: Subscription;
  public showExport = true;
  private queryCollectionSource = new ReplaySubject<UserQuery[]>(1);
  public queryCollection$: Observable<UserQuery[]> = this.queryCollectionSource.asObservable();
  routeSubscription: Subscription;

  private changeSubscription: Subscription;
  private removeSubscription: Subscription;
  private tableSubscription: Subscription;
  customParams: any = {};
  public activeEntity: any;
  
  public  includeProperties: string[];

  public query: any = {
    type: 'LogicalExpression',
    operator: '$and',
    elements: [
      {
        type: 'BinaryExpression',
        operator: '$eq',
        left: null,
        right: null
      }
    ]
  };


  constructor(private _activatedRoute: ActivatedRoute,
              private _appEvent: AppEventService,
              public activatedTable: ActivatedTableService,
              public activeQuery: ActiveTableQueryBuilder,
              private applicationDatabase: ApplicationDatabase,
              private translateService: TranslateService) { }


  ngAfterViewInit(): void {

        this.applicationDatabase.db.table('UserQuery').where('entityType').equals(`${this.activeEntity}`).toArray().then((items) => {
          this.queryCollectionSource.next(items);
        });
      
        this.changeSubscription = this._appEvent.changed.subscribe((event: { model: string; target: UserQuery }) => {
          if (event.model === 'UserQuery') {
            this.applicationDatabase.db.table('UserQuery').where('entityType').equals(`${this.activeEntity}`).toArray().then((items) => {
              this.queryCollectionSource.next(items);
            });
          }
        });
      
        this.removeSubscription = this._appEvent.removed.subscribe((event: { model: string; target: UserQuery }) => {
          if (event.model === 'UserQuery') {
            this.applicationDatabase.db.table('UserQuery').where('entityType').equals(`${this.activeEntity}`).toArray().then((items) => {
              this.queryCollectionSource.next(items);
            });
          }
        });
   
        this.table = this.activatedTable.activeTable;
        this.tableSubscription = this.table.configChanges.subscribe((config) => {
          if (config) {
            this.activeEntity = config.model === 'StudentPeriodRegistrations' ? 'StudentPeriodRegistration' : 'Student';
            this.includeProperties = config.model === 'StudentPeriodRegistrations' ?  cloneDeep(registrationBuilder).properties.map((item) => item.name) : cloneDeep(studentBuilder).properties.map((item) => item.name);
          }
        });
        
        this.routeSubscription = this._activatedRoute.queryParams.subscribe((queryParams) => {
          this.activeQuery.find(queryParams).subscribe((item) => {
            if (item) {
              this.activeQuery.query = item.query;
            }
          });
        });
    
  }    
  
  
  ngOnInit() {

    this.toggleShowExportSubscription = this._appEvent.changed.subscribe(change => {
      if (change && change.model === 'ToggleShowExport') {
        this.showExport = change.target;
      }
    });

    this.paths = (<any>REGISTRATIONS_LIST_CONFIG).paths;
    this.activePaths = this.paths.filter( x => {
      return x.show === true;
    }).slice(0);
    this.paramSubscription = this._activatedRoute.firstChild.params.subscribe( params => {
      const matchPath = new RegExp('^list/' + params.list  + '$', 'ig');
      const findItem = this.paths.find( x => {
        return matchPath.test(x.alternateName);
      });
      if (findItem) {
        this.showTab(findItem);
      }
    });

  }

  showTab(item: any) {
    // find if path exists in active paths
    const findIndex = this.activePaths.findIndex( x => {
      return x.alternateName === item.alternateName;
    });
    if (findIndex < 0) {
      if (this.activePaths.length >= this.maxActiveTabs) {
        // remove item at index 0
        this.activePaths.splice(0, 1);
      }
      // add active path
      this.activePaths.push(Object.assign({}, item));
    }
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.toggleShowExportSubscription) {
      this.toggleShowExportSubscription.unsubscribe();
    }
    if (this.removeSubscription) {
      this.removeSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
    if (this.tableSubscription) {
      this.tableSubscription.unsubscribe();
    }
  }

  onLoadingProperties(properties: QueryProperty[]) {
  let builder = this.table.config.model === "StudentPeriodRegistrations" ? registrationBuilder : studentBuilder ; 
  properties.forEach((property) => {
    const find = builder.properties.find((item) => {
      return '$'.concat(item.name) === property.value
    })
    if (find) {
      Object.assign(property, {
        label: this.translateService.instant(find.label),
      });
      if (find.source) {
        Object.assign(property, {
          source: find.source
        });
      }
      if (find.template) {
        Object.assign(property, {
          template: find.template
        });
      }
    }
  });

  // add missing properties
  builder.properties.forEach((property) => {
    const find = properties.find((item) => {
      return '$'.concat(property.name) === item.value
    })
    if (find == null) {
      properties.push({
        label: this.translateService.instant(property.label),
        value: `${property.name}`,
        type: property.type,
        source: property.source,
        template: property.template
      });
    }
  });
    
    // handling of zero-or-one navigation property
    const departmentSnapshot = properties.find((property) => property.value === '$departmentSnapshot');
    if (departmentSnapshot) {
      departmentSnapshot.value = '$departmentSnapshot/id';
    }
    const studentSeries = properties.find((property) => property.value === '$studentSeries');
    if (studentSeries) {
      studentSeries.value = '$studentSeries/id';
    }

  }

}
