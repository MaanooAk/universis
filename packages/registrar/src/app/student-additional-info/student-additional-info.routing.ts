import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentAdditionalInfoTableComponent } from './components/student-additional-info-table/student-additional-info-table.component';
import { AdvancedFormItemResolver,
  AdvancedFormModalData,
  AdvancedFormModalComponent
} from '@universis/forms';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'list'
      },
      {
        path: 'list',
        data: {
          title: 'StudentInformation'
        },
        component: StudentAdditionalInfoTableComponent,
        children: [
          {
            path: ':id/edit',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData> {
              model: 'StudentInformations',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'infoType,student($expand=person)',
              }
            },
            resolve: {
              data: AdvancedFormItemResolver
            }
          }
        ]
      }
    ]
  },
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class StudentAdditionalInfoRoutingModule {
}
