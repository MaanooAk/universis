import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';

@Component({
  selector: 'app-dashboard-exam-participate-summary',
  templateUrl: './dashboard-exam-participate-summary.component.html',
  styleUrls: ['./dashboard-exam-participate-summary.component.scss']
})
export class DashboardExamParticipateSummaryComponent implements OnInit {

  public participateInfo: any;
  public department: any;

  constructor(private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService) {
  }

  async ngOnInit() {
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    // get department configuration
    this.department = await this._context.model('Departments')
      .select('departmentConfiguration')
      .expand('departmentConfiguration($expand=examYear, examPeriod)')
      .where('id').equal(activeDepartment.id).getItem();
    if (this.department && this.department.departmentConfiguration) {
      // get exam year and period
      const examYear = this.department.departmentConfiguration.examYear;
      const examPeriod = this.department.departmentConfiguration.examPeriod;
      if (examYear && examPeriod) {
        // build participate info
        // get active students
        const activeStudents = await this._context.model('Students')
          .where('studentStatus/alternateName ').equal('active')
          .and('department').equal(activeDepartment.id)
          .select('count(id) as totalActive')
          .getItem();
        // get exam participate requests for specified year period
        const participates = await this._context.model('ExamPeriodParticipateActions')
          .where('student/department').equal(activeDepartment.id)
          .and('year').equal(examYear.id)
          .and('examPeriod').equal(examPeriod.id)
          .groupBy('agree')
          .select('count(id) as count, agree')
          .getItems();
        const accepted = (participates || []).find(x => {
          return x.agree === true;
        });
        const declined = (participates || []).find(x => {
          return x.agree === false;
        });
        this.participateInfo = {
          'active': activeStudents ? activeStudents.totalActive : 0,
          'agree': accepted ? accepted.count : 0,
          'decline': declined ? declined.count : 0
        };
      }
    }
  }
}
