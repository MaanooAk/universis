import { AfterViewInit, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';
import { AdvancedFormsService, ServiceUrlPreProcessor } from '@universis/forms';
import { BehaviorSubject, from } from 'rxjs';
import {divideClassForm} from './divideClass.form';
import { cloneDeep } from 'lodash';
import { fadeAnimation } from '../../../../animations';

@Component({
  selector: 'one-roster-divide-class',
  animations: [
    fadeAnimation
  ],
  templateUrl: './one-roster-divide-class.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./one-roster-class-configuration.component.scss']
})
export class OneRosterDivideClassComponent implements OnChanges, AfterViewInit, OnDestroy {

  @Input('courseClass') courseClass: {
     id: string,
     title: string,
     department: string,
     displayCode: string,
     year: string,
     period: string,
     sections: any[]
  };

  public form: any;
  public dividedInto$ = new BehaviorSubject([]);
  private removed: any[] = [];
  renderOptions: { language: string; i18n: { [x: string]: any; }; };
  isValid = true;

  public get dividedInto$$() {
    return from(
        this.context
          .model('OneRosterDivideClasses')
          .where('courseClass')
          .equal(this.courseClass.id)
          .expand('instructors($select=id,familyName,givenName),courseClass')
          .getItems()
    );
  }

  constructor(
    private context: AngularDataContext,
    private activatedRoute: ActivatedRoute,
    private errorService: ErrorService,
    private loadingService: LoadingService,
    private formService: AdvancedFormsService,
    private _translateService: TranslateService
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.courseClass && changes.courseClass.currentValue) {
      const form = cloneDeep(divideClassForm);
      new ServiceUrlPreProcessor(this.context).parse(form);
        const { currentLang: language } = this._translateService;
        this.renderOptions = {
          language,
          i18n: {
            [language]: form.settings.i18n[language]
          }
        };
        this.form = form;
        this.dividedInto$$.subscribe((items) => {
          if (items.length === 0) {
            //
          }
          this.dividedInto$.next(items);
        });
    }
  }
  ngOnDestroy(): void {
    //
  }
  ngAfterViewInit(): void {

  }

  onEvent(event: { changed?: any; data?: any; isValid: boolean, type?: string }, items: any[], index: number) {
    if (event.type === 'cancel') {
      this.remove(items, index);
      this.isValid = true;
    }
    if (event.type === 'remove') {
      const [item] = items.splice(index, 1);
      if (item.id) {
        this.removed.push(item);
      }
      this.isValid = true;
    }
  }

  onChange(event: { changed?: any; data?: any; isValid: boolean }, item: any) {
    if (typeof event.changed === 'undefined') {
      return;
    }
    if (event.changed) {
      this.isValid = event.isValid;
    }
    console.log(event);
    Object.assign(item, event.data);
  }

  add(items: any[]) {
    items.push({
      courseClass: this.courseClass
    });
    this.isValid = false;
  }

  remove(items: any[], index: number) {
    const remove = items.splice(index, 1);
    if (remove[0].id) {
      this.removed.push(remove[0]);
    }
    this.isValid = true;
  }

  save(items: any[]) {
    this.loadingService.showLoading();
    // get existing items
    this.context
      .model('OneRosterDivideClasses')
      .where('courseClass')
      .equal(this.courseClass.id)
      .expand('instructors')
      .getItems().then((dividedInto) => {
      // set removed items to inactive
      dividedInto.forEach((item: any) => {
        // find if this item is removed by user
        const removed = this.removed.findIndex((x) => x.id === item.id);
        // assing status
        Object.assign(item, {
          status: removed >= 0 ? 'inactive' : item.status
        });
        // if the item is not getting removed
        if (removed === -1) {
          // get the form item that corresponds to the division
          const formItem = items.filter(element => element.id === item.id)[0];
          // get the instructors from the form item
          const instructors = formItem.instructors;
          // push the division instructors that got removed with $state = 4
          for (const instructor of item.instructors) {
            if (!instructors.find(element => element.id === instructor.id)) {
              instructor.$state = 4;
              instructors.push(instructor);
            }
          }
          // assign the two form fields
          Object.assign(item, {
            title: formItem.title,
            instructors
          });
        }
      });

      // add new items
      let index = dividedInto.length;
      // find items that are not saved
      const insert = items.filter((item) => !item.id).map((item) => {
        index++;
        // set class index
        const instructors = item.instructors;
        return Object.assign(item, {
          instructors,
          classIndex: index
        });
      });
      // pust new items to existing items
      dividedInto.push(...insert);
      void this.context.model('OneRosterDivideClasses').save(dividedInto).then(() => {
        this.loadingService.hideLoading();
        items.forEach((item, index) => {
          Object.assign(item, {
            classIndex: index + 1
          });
        });
      });
    }).then(() => {
      this.loadingService.hideLoading();
    }).catch((err) => {
      console.error(err);
      this.loadingService.hideLoading();
      this.errorService.showError(err, {
        continueLink: '.',
      });
    });
  }

  onFormLoad() {

  }


}
