import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-graduations-requests-root',
  templateUrl: './graduations-requests-root.component.html'
})
export class GraduationsRequestsRootComponent implements OnInit {

  public model: any;
  public tabs: any[];
  public graduationRequestId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.graduationRequestId = this._activatedRoute.snapshot.params.requestId;

    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

    this.model = await this._context.model('GraduationRequestActions')
      .where('id').equal(this.graduationRequestId)
      .expand('graduationEvent,student($expand=person)')
      .getItem();
  }
}
