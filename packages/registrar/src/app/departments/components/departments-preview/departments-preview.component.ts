import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';

@Component({
  selector: 'app-departments-preview',
  templateUrl: './departments-preview.component.html'
})
export class DepartmentsPreviewComponent implements OnInit {

  @Input() departments: any;

   constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private translate: TranslateService,
               private _activeDepartmentService: ActiveDepartmentService) {
  }

  async ngOnInit() {

    this.departments = await this._context.model('LocalDepartments')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .getItem();

    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    if(this.departments.id === activeDepartment.id){
      if(!this._activeDepartmentService.checkDepartmentsEqual(activeDepartment, this.departments)){
        this._activeDepartmentService.setActiveDepartment(this.departments);
      }
    }
  }

}
