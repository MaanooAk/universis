import { ModuleWithProviders, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import {StudentsSharedModule} from '../students/students.shared';
import { CoursesSharedModule } from '../courses/courses.shared';
import { ExamsPreviewFormComponent } from './components/exams-preview-general/exams-preview-form.component';
import { ExamMessageSubscriber } from './services/ExamMessageSubscriber';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        FormsModule,
        StudentsSharedModule,
        CoursesSharedModule
    ],
    declarations: [
        ExamsPreviewFormComponent
    ],
    exports: [
        ExamsPreviewFormComponent
    ],
    providers: [
    ]
})
export class ExamsSharedModule implements OnInit {
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch((err) => {
            console.error('An error occurred while loading exams shared module');
            console.error(err);
        });
    }

    static forRoot(): ModuleWithProviders<ExamsSharedModule> {
        return {
            ngModule: ExamsSharedModule,
            providers: [
                ExamMessageSubscriber
            ]
        };
    }

    async ngOnInit() {
        environment.languages.forEach((language) => {
            import(`./i18n/exams.${language}.json`).then((translations) => {
                this._translateService.setTranslation(language, translations, true);
            });
        });
    }
}
