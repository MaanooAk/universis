import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdvancedSearchComponent} from './components/advanced-search/advanced-search.component';
import {DropdownFormComponent} from './components/dropdown-form/dropdown-form.component';
import {ExpandableFormComponent} from './components/expandable-form/expandable-form.component';
import {SimpleFormComponent} from './components/simple-form/simple-form.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'advanced-search'
  },
  {
    path: 'advanced-search',
    component: AdvancedSearchComponent,
    data: {
      title: 'Advanced Search'
    }
  },
  {
    path: 'dropdown-form',
    component: DropdownFormComponent,
    data: {
      title: 'Dropdown Form'
    }
  },
  {
    path: 'expandable-form',
    component: ExpandableFormComponent,
    data: {
      title: 'Expandable Form'
    }
  },
  {
    path: 'simple-form',
    component: SimpleFormComponent,
    data: {
      title: 'Simple Form'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule {
  constructor() { }
}
